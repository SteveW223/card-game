﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace CardGame
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class StartPage : Page
    {
        string backColour;

        public StartPage()
        {
            this.InitializeComponent();
        }

        private void Blue_Tapped(object sender, TappedRoutedEventArgs e)
        {
            backColour = "Blue";
            txt_BackColour.Text = backColour;
        }

        private void Red_Tapped(object sender, TappedRoutedEventArgs e)
        {
            backColour = "Red";
            txt_BackColour.Text = backColour;
        }

        private void Green_Tapped(object sender, TappedRoutedEventArgs e)
        {
            backColour = "Green";
            txt_BackColour.Text = backColour;
        }

        private void Gray_Tapped(object sender, TappedRoutedEventArgs e)
        {
            backColour = "Gray";
            txt_BackColour.Text = backColour;
        }

        private void Purple_Tapped(object sender, TappedRoutedEventArgs e)
        {
            backColour = "Purple";
            txt_BackColour.Text = backColour;
        }

        private void Yellow_Tapped(object sender, TappedRoutedEventArgs e)
        {
            backColour = "Yellow";
            txt_BackColour.Text = backColour;
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage), backColour);
        }
    }
}
