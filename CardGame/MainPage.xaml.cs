﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using CardGame.GameCore;
using Windows.UI.Popups;
using Windows.ApplicationModel.Core;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace CardGame
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        GameCore.Players.Player Human = new GameCore.Players.Player();
        GameCore.Players.GLaDoS Comp = new GameCore.Players.GLaDoS();
        CardGen.Card compCard = new CardGen.Card();
        int shuffles = 0;
        bool gameStarted = false;

        bool card1Picked = false;
        bool card2Picked = false;
        bool card3Picked = false;

        int cardsPicked = 0;

        string backColour;
        string backCode;

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            backColour = e.Parameter.ToString();

            switch (backColour)
            {
                case "Blue":
                    backCode = "BCB";
                    break;
                case "Red":
                    backCode = "BCR";
                    break;
                case "Green":
                    backCode = "BCG";
                    break;
                case "Gray":
                    backCode = "BKG";
                    break;
                case "Purple":
                    backCode = "BCP";
                    break;
                case "Yellow":
                    backCode = "BCY";
                    break;
                default:
                    break;
            }

            PlayerCard.DisplayCard(backCode);
            ComputCard.DisplayCard(backCode);
            Deck.RegenDeck();

            base.OnNavigatedTo(e);
        }

        public MainPage()
        {
            this.InitializeComponent();
            Human.ShuffleDeck();
            DisplayCards();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(shuffles < 3 && gameStarted==false)
            {
                Human.ShuffleDeck();
                DisplayCards();
                shuffles++;
            }
            else
            {
                ShuffleBtn.IsEnabled = false;
            }
        }
        private void DisplayCards()
        {
            Card1.DisplayCard(Human.MyCardList[0].CardCode);
            Card2.DisplayCard(Human.MyCardList[1].CardCode);
            Card3.DisplayCard(Human.MyCardList[2].CardCode);
        }

        private void Card1_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if(!card1Picked)
            {
                PlayerCard.DisplayCard(Human.MyCardList[0].CardCode);
                gameStarted = true;
                Card1.DisplayCard(backCode);
                card1Picked = true;
                cardsPicked++;
                compCard = Comp.SelectCard();
                ComputCard.DisplayCard(compCard.CardCode);
                int roundScore = (Human.MyCardList[0].Value + compCard.Value) * 10;
                Human.Score += roundScore;
                Comp.Score += roundScore;
                

                if (Human.MyCardList[0].Value > compCard.Value)
                {
                  Result.Text = "You win";
                    humanRndScore.Text = (roundScore).ToString();
                    totalHumanScore.Text = (Human.Score).ToString();
                }
                else
                {
                    
                    Result.Text = "THE COMPUTER WINS";
                    compRndScore.Text = roundScore.ToString();
                    totalCompScore.Text = (Comp.Score).ToString();
                }
                EndGame();
            }
        }

        private void Card2_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (!card2Picked)
            {
                PlayerCard.DisplayCard(Human.MyCardList[1].CardCode);
                gameStarted = true;
                Card2.DisplayCard(backCode);
                card2Picked = true;
                cardsPicked++;
                compCard = Comp.SelectCard();
                ComputCard.DisplayCard(compCard.CardCode);
                int roundScore = (Human.MyCardList[1].Value + compCard.Value) * 10;
                Human.Score += roundScore;
                Comp.Score += roundScore;
                if (Human.MyCardList[1].Value > compCard.Value)
                {
                    Result.Text = "You win";

                    humanRndScore.Text = (roundScore).ToString();
                    totalHumanScore.Text = (Human.Score).ToString();
                }
                else
                {
                    Result.Text = "THE COMPUTER WINS";
                    compRndScore.Text = Comp.Score.ToString();
                    totalCompScore.Text = (Comp.Score).ToString();

                }
                EndGame();
            }
        }

        private void Card3_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (!card3Picked)
            {
                PlayerCard.DisplayCard(Human.MyCardList[2].CardCode);
                gameStarted = true;
                Card3.DisplayCard(backCode);
                card3Picked = true;
                cardsPicked++;
                compCard = Comp.SelectCard();
                ComputCard.DisplayCard(compCard.CardCode);
                int roundScore = (Human.MyCardList[2].Value + compCard.Value) * 10;
                Human.Score += roundScore;
                Comp.Score += roundScore;
                

                if (Human.MyCardList[2].Value > compCard.Value)
                {
                    Result.Text = "You Win";
                    humanRndScore.Text = (roundScore).ToString();
                    totalHumanScore.Text = (Human.Score).ToString();
                }
                else
                {
                    Result.Text = "THE COMPUTER WINS";
                    compRndScore.Text = roundScore.ToString();
                    totalCompScore.Text = (Comp.Score).ToString();

                }
                EndGame();
            }
        }

        public void EndGame()
        {
            if (cardsPicked == 3)
            {
                if(Int32.Parse(totalCompScore.Text) > Int32.Parse(totalHumanScore.Text))
                {
                    this.Frame.Navigate(typeof(ResultsPage), new Winner("Computer", Int32.Parse(totalCompScore.Text)));
                }
                else if(Int32.Parse(totalCompScore.Text) < Int32.Parse(totalHumanScore.Text))
                {
                    this.Frame.Navigate(typeof(ResultsPage), new Winner("Player", Int32.Parse(totalHumanScore.Text)));
                }
                else
                {
                    this.Frame.Navigate(typeof(ResultsPage), new Winner("Everyone", Int32.Parse(totalHumanScore.Text)));
                }
            }
        }
    }
}
