﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGame.CardGen
{
    public class Card
    {
        public int Value { get; set; } //A(1), 2, 3, 4, 5, 6, 7, 8, 9, 10, J(11), Q(12), K(13)
        public CardType Suit { get; set; }
        public string CardCode { get; set; }
        public Card()
        {

        }
        public Card(int value, CardType suit, string cardCode)
        {
            Value = value;
            Suit = suit;
            CardCode = cardCode;
        }
    }
    public enum CardType
    {
        Heart,
        Spade,
        Club,
        Diamond
    };

   
}