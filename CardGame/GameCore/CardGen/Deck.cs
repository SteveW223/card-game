﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGame
{
    public class Deck
    {
        public static List<CardGen.Card> CardList { get; set; }

        static Deck()
        {
            CardList = new List<CardGen.Card>();

            for (int i = 1; i <= 13; i++)
            {
                CardList.Add(new CardGen.Card(i, CardGen.CardType.Club, i + "C"));
                CardList.Add(new CardGen.Card(i, CardGen.CardType.Diamond, i + "D"));
                CardList.Add(new CardGen.Card(i, CardGen.CardType.Heart, i + "H"));
                CardList.Add(new CardGen.Card(i, CardGen.CardType.Spade, i + "S"));
            }
        }

        public static void RegenDeck()
        {
            CardList = new List<CardGen.Card>();

            for (int i = 1; i <= 13; i++)
            {
                CardList.Add(new CardGen.Card(i, CardGen.CardType.Club, i + "C"));
                CardList.Add(new CardGen.Card(i, CardGen.CardType.Diamond, i + "D"));
                CardList.Add(new CardGen.Card(i, CardGen.CardType.Heart, i + "H"));
                CardList.Add(new CardGen.Card(i, CardGen.CardType.Spade, i + "S"));
            }
        }

        public static List<CardGen.Card> DealCards()
        {
            List<CardGen.Card> _playerHand = new List<CardGen.Card>();
            Random cardGen = new Random();

            for (int i = 0; i < 3; i++)
            {
                int num = cardGen.Next(0, CardList.Count);
                _playerHand.Add(CardList[num]);
                CardList.Remove(CardList[num]);
            }
            return _playerHand;
        }
    }
}
