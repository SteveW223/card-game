﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGame.GameCore.Players
{
    public class GLaDoS
    {
        public List<CardGen.Card> MyCardList { get; set; }
        public int Score { get; set; }

        public GLaDoS()
        {
            MyCardList = Deck.DealCards();
        }
       public CardGen.Card SelectCard()
        {
            CardGen.Card returnCard = new CardGen.Card();

            int value = 0;
            int cardIndex = 0;
            for (int i = 0; i < MyCardList.Count; i++)
            {
                if (MyCardList[i].Value > value)
                {
                    value = MyCardList[i].Value;
                    cardIndex = i;
                }
            }
            returnCard.Suit = MyCardList[cardIndex].Suit;
            returnCard.Value = MyCardList[cardIndex].Value;
            returnCard.CardCode = MyCardList[cardIndex].CardCode;
            MyCardList.Remove(MyCardList[cardIndex]);
            return (returnCard);

        }
    }
}