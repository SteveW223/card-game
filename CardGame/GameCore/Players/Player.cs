﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGame.GameCore.Players
{
    public class Player
    {
        public List<CardGen.Card> MyCardList { get; set; }
        public int Score { get; set; }

        public Player()
        {
            MyCardList = Deck.DealCards();
        }

        public void ShuffleDeck()
        {
            MyCardList = Deck.DealCards();
        }
    }
}