﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGame.GameCore
{
    class Winner
    {
        public string WinningPlayer { get; set; }
        public int WinningScore { get; set; }

        public Winner(string name, int score)
        {
            WinningPlayer = name;
            WinningScore = score;
        }
    }
}
