﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace CardGame
{
    public sealed partial class Card : UserControl
    {
        public Card()
        {
            this.InitializeComponent();
        }

        public void HideAllCards()
        {
            //Hide Clubs
            CardAC.Visibility = Visibility.Collapsed;
            Card2C.Visibility = Visibility.Collapsed;
            Card3C.Visibility = Visibility.Collapsed;
            Card4C.Visibility = Visibility.Collapsed;
            Card5C.Visibility = Visibility.Collapsed;
            Card6C.Visibility = Visibility.Collapsed;
            Card7C.Visibility = Visibility.Collapsed;
            Card8C.Visibility = Visibility.Collapsed;
            Card9C.Visibility = Visibility.Collapsed;
            Card10C.Visibility = Visibility.Collapsed;
            CardJC.Visibility = Visibility.Collapsed;
            CardQC.Visibility = Visibility.Collapsed;
            CardKC.Visibility = Visibility.Collapsed;
            //Hide Hearts
            CardAH.Visibility = Visibility.Collapsed;
            Card2H.Visibility = Visibility.Collapsed;
            Card3H.Visibility = Visibility.Collapsed;
            Card4H.Visibility = Visibility.Collapsed;
            Card5H.Visibility = Visibility.Collapsed;
            Card6H.Visibility = Visibility.Collapsed;
            Card7H.Visibility = Visibility.Collapsed;
            Card8H.Visibility = Visibility.Collapsed;
            Card9H.Visibility = Visibility.Collapsed;
            Card10H.Visibility = Visibility.Collapsed;
            CardJH.Visibility = Visibility.Collapsed;
            CardQH.Visibility = Visibility.Collapsed;
            CardKH.Visibility = Visibility.Collapsed;
            //Hide Diamonds
            CardAD.Visibility = Visibility.Collapsed;
            Card2D.Visibility = Visibility.Collapsed;
            Card3D.Visibility = Visibility.Collapsed;
            Card4D.Visibility = Visibility.Collapsed;
            Card5D.Visibility = Visibility.Collapsed;
            Card6D.Visibility = Visibility.Collapsed;
            Card7D.Visibility = Visibility.Collapsed;
            Card8D.Visibility = Visibility.Collapsed;
            Card9D.Visibility = Visibility.Collapsed;
            Card10D.Visibility = Visibility.Collapsed;
            CardJD.Visibility = Visibility.Collapsed;
            CardQD.Visibility = Visibility.Collapsed;
            CardKD.Visibility = Visibility.Collapsed;
            //Hide Spades
            CardAS.Visibility = Visibility.Collapsed;
            Card2S.Visibility = Visibility.Collapsed;
            Card3S.Visibility = Visibility.Collapsed;
            Card4S.Visibility = Visibility.Collapsed;
            Card5S.Visibility = Visibility.Collapsed;
            Card6S.Visibility = Visibility.Collapsed;
            Card7S.Visibility = Visibility.Collapsed;
            Card8S.Visibility = Visibility.Collapsed;
            Card9S.Visibility = Visibility.Collapsed;
            Card10S.Visibility = Visibility.Collapsed;
            CardJS.Visibility = Visibility.Collapsed;
            CardQS.Visibility = Visibility.Collapsed;
            CardKS.Visibility = Visibility.Collapsed;
            //Hide Misc
            BackBlue.Visibility = Visibility.Collapsed;
            BackGreen.Visibility = Visibility.Collapsed;
            BackGray.Visibility = Visibility.Collapsed;
            BackYellow.Visibility = Visibility.Collapsed;
            BackRed.Visibility = Visibility.Collapsed;
            BackPurple.Visibility = Visibility.Collapsed;
        }
        public void DisplayCard (string cardValue)
        {
            HideAllCards();
            switch (cardValue)
            {
                //Back
                case "BCB":
                    BackBlue.Visibility = Visibility.Visible;
                    break;
                case "BCR":
                    BackRed.Visibility = Visibility.Visible;
                    break;
                case "BCG":
                    BackGreen.Visibility = Visibility.Visible;
                    break;
                case "BKG":
                    BackGray.Visibility = Visibility.Visible;
                    break;
                case "BCP":
                    BackPurple.Visibility = Visibility.Visible;
                    break;
                case "BCY":
                    BackYellow.Visibility = Visibility.Visible;
                    break;
                //Clubs
                case "1C":
                    CardAC.Visibility = Visibility.Visible;
                    break;
                case "2C":
                    Card2C.Visibility = Visibility.Visible;
                    break;
                case "3C":
                    Card3C.Visibility = Visibility.Visible;
                    break;
                case "4C":
                    Card4C.Visibility = Visibility.Visible;
                    break;
                case "5C":
                    Card5C.Visibility = Visibility.Visible;
                    break;
                case "6C":
                    Card6C.Visibility = Visibility.Visible;
                    break;
                case "7C":
                    Card7C.Visibility = Visibility.Visible;
                    break;
                case "8C":
                    Card8C.Visibility = Visibility.Visible;
                    break;
                case "9C":
                    Card9C.Visibility = Visibility.Visible;
                    break;
                case "10C":
                    Card10C.Visibility = Visibility.Visible;
                    break;
                case "11C":
                    CardJC.Visibility = Visibility.Visible;
                    break;
                case "12C":
                    CardQC.Visibility = Visibility.Visible;
                    break;
                case "13C":
                    CardKC.Visibility = Visibility.Visible;
                    break;
                //Hearts
                case "1H":
                    CardAH.Visibility = Visibility.Visible;
                    break;
                case "2H":
                    Card2H.Visibility = Visibility.Visible;
                    break;
                case "3H":
                    Card3H.Visibility = Visibility.Visible;
                    break;
                case "4H":
                    Card4H.Visibility = Visibility.Visible;
                    break;
                case "5H":
                    Card5H.Visibility = Visibility.Visible;
                    break;
                case "6H":
                    Card6H.Visibility = Visibility.Visible;
                    break;
                case "7H":
                    Card7H.Visibility = Visibility.Visible;
                    break;
                case "8H":
                    Card8H.Visibility = Visibility.Visible;
                    break;
                case "9H":
                    Card9H.Visibility = Visibility.Visible;
                    break;
                case "10H":
                    Card10H.Visibility = Visibility.Visible;
                    break;
                case "11H":
                    CardJH.Visibility = Visibility.Visible;
                    break;
                case "12H":
                    CardQH.Visibility = Visibility.Visible;
                    break;
                case "13H":
                    CardKH.Visibility = Visibility.Visible;
                    break;
                //Diamonds
                case "1D":
                    CardAD.Visibility = Visibility.Visible;
                    break;
                case "2D":
                    Card2D.Visibility = Visibility.Visible;
                    break;
                case "3D":
                    Card3D.Visibility = Visibility.Visible;
                    break;
                case "4D":
                    Card4D.Visibility = Visibility.Visible;
                    break;
                case "5D":
                    Card5D.Visibility = Visibility.Visible;
                    break;
                case "6D":
                    Card6D.Visibility = Visibility.Visible;
                    break;
                case "7D":
                    Card7D.Visibility = Visibility.Visible;
                    break;
                case "8D":
                    Card8D.Visibility = Visibility.Visible;
                    break;
                case "9D":
                    Card9D.Visibility = Visibility.Visible;
                    break;
                case "10D":
                    Card10D.Visibility = Visibility.Visible;
                    break;
                case "11D":
                    CardJD.Visibility = Visibility.Visible;
                    break;
                case "12D":
                    CardQD.Visibility = Visibility.Visible;
                    break;
                case "13D":
                    CardKD.Visibility = Visibility.Visible;
                    break;
                //Spades
                case "1S":
                    CardAS.Visibility = Visibility.Visible;
                    break;
                case "2S":
                    Card2S.Visibility = Visibility.Visible;
                    break;
                case "3S":
                    Card3S.Visibility = Visibility.Visible;
                    break;
                case "4S":
                    Card4S.Visibility = Visibility.Visible;
                    break;
                case "5S":
                    Card5S.Visibility = Visibility.Visible;
                    break;
                case "6S":
                    Card6S.Visibility = Visibility.Visible;
                    break;
                case "7S":
                    Card7S.Visibility = Visibility.Visible;
                    break;
                case "8S":
                    Card8S.Visibility = Visibility.Visible;
                    break;
                case "9S":
                    Card9S.Visibility = Visibility.Visible;
                    break;
                case "10S":
                    Card10S.Visibility = Visibility.Visible;
                    break;
                case "11S":
                    CardJS.Visibility = Visibility.Visible;
                    break;
                case "12S":
                    CardQS.Visibility = Visibility.Visible;
                    break;
                case "13S":
                    CardKS.Visibility = Visibility.Visible;
                    break;
            }
        }
    }
}